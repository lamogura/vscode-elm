### 0.11.2 - 10.05.2017
* Improved Linter so that issues are associated with the correct files
* Fix for elm-oracle being fed entire elm file

### 0.11.1 - 10.05.2017
* Fix keyboard shortcuts

### 0.11.0 - 08.05.2017
* Add `Elm: Browse package` command
* Fix paths concatenation issue for non-Windows machines

### 0.10.0 - 05.05.2017
